<?php get_header(); ?>
<section id="notfoundwrap">
<section class="notfoundinner notfoundstroke">
    <?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?>
</section>
<section class="notfoundinner notfoundfill notfoundfill-red">
    <a class="logo" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?></a>
</section>
<section class="notfoundinner notfoundfill notfoundfill-green">
    <a class="logo" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?></a>
</section>
<section class="notfoundinner notfoundfill notfoundfill-blue">
    <a class="logo" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?></a>
</section>
<section class="notfoundinner notfoundfill notfoundfill-white">
    <a class="logo" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?></a>
</section>
<p>404: Page not found.</p>
</section>
<?php get_footer(); ?>  