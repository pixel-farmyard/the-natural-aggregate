<section class="featured-posts">
    <?php
    $test = array(get_field('featured_post_one'),get_field('featured_post_two'),get_field('featured_post_three'));
    foreach ($test as $post ) : setup_postdata( $post ); 
                if (has_post_thumbnail()) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumb-size' );
                $thumbnail_url = $thumbnail_data[0];
                }
                ?>
    <div class="featured-post-inner">
        <div class="image-box" style="background-image:url('<?php echo $thumbnail_url ?>');"></div>
        <div class="text-wrap">
    	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    	<?php the_excerpt(); ?>
    	<a class="button" href="<?php the_permalink(); ?>">Read</a>
        </div>
    </div>
    <?php endforeach; wp_reset_postdata(); ?>
</section>