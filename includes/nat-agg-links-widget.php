<style type="text/css">
    #nat-agg-links-widget {
        background-color:#2d2d2d;
        color:white;
    }
    
    #nat-agg-links-widget .hndle {
        background-color:white;
    }
    
    #nat-agg-links-widget .inside {
        background-image:url('http://thenaturalaggregate.co.uk/wp-content/themes/thenaturalaggregate/assets/500-500-on-grey.jpg');
        background-repeat:no-repeat;
        background-position:top right;
        background-size:30%;
    }
    
    #nat-agg-links-widget .inside ul {
        padding-left:3em;
        font-weight:bold;
        line-height:2em;
    }
    
    #nat-agg-links-widget .inside ul li a {
        color:white;
        transition:all 0.5s ease;
        line-height:2em;
    }
    
    #nat-agg-links-widget .inside ul li a:hover {
        color:#ccc;
    }
    
    #nat-agg-links-widget .inside ul li a:hover::before {
        left:-15px;
    }
    
    #nat-agg-links-widget .inside li a::before {
        transition:all 0.5s ease;
        content:"";
        position:relative;
        left:-20px;
        top:6px;
        display:inline-block;
        width:24px;
        height:24px;
        background:url('../wp-content/themes/thenaturalaggregate/assets/generic-link.png') no-repeat center center;
        background-size:70%;
    }
    
    #nat-agg-links-widget .inside li a.vimeo-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/vimeo-icon.png')
    }
    #nat-agg-links-widget .inside li a.youtube-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/youtube-icon.png')
    }
    #nat-agg-links-widget .inside li a.facebook-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/facebook-icon.png')
    }
    #nat-agg-links-widget .inside li a.twitter-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/twitter-icon.png')
    }
    #nat-agg-links-widget .inside li a.instagram-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/instagram-icon.png')
    }
    #nat-agg-links-widget .inside li a.soundcloud-link::before{
        background-image:url('../wp-content/themes/thenaturalaggregate/assets/soundcloud-icon.png')
    }
    
    
</style>

<p>Some helpful links for The Natural Aggregate:</p>
<ul>
    <li><a class="nat-agg-link" href="https://www.instagram.com/thenataggregate/" target="_blank">Instagram (@thenataggregate)</a></li>
    <li><a class="nat-agg-link" href="https://twitter.com/TheNatAggregate" target="_blank">Twitter (@thenataggregate)</a></li>
    <li><a class="nat-agg-link" href="https://www.facebook.com/TheNaturalAggregate" target="_blank">Facebook Page</a></li>
    <li><a class="nat-agg-link" href="https://vimeo.com/thenaturalaggregate" target="_blank">Vimeo</a></li>
    <li><a class="nat-agg-link" href="https://www.youtube.com/channel/UCHgi3YrsBBo_8AgB0TcDkGg" target="_blank">YouTube</a></li>
    <li><a class="nat-agg-link" href="https://thenaturalaggregate.bandcamp.com/" target="_blank">Bandcamp</a></li>
    <li><a class="nat-agg-link" href="https://soundcloud.com/the-natural-aggregate" target="_blank">Soundcloud</a></li>
</ul>

<script type="text/javascript">
    var links = document.getElementsByClassName("nat-agg-link");    
    var arrayLength = links.length;
        for (var i = 0; i < arrayLength; i++) {
            var naurls = links[i].getAttribute('href');
            console.log(naurls);
            if (naurls.includes('vimeo')) {
                links[i].classList.add('vimeo-link');
            } else if (naurls.includes('youtu')) {
                links[i].classList.add('youtube-link');
            } else if (naurls.includes('facebook')) {
                links[i].classList.add('facebook-link');
            } else if (naurls.includes('twitter')) {
                links[i].classList.add('twitter-link');
            } else if (naurls.includes('instagram')){
                links[i].classList.add('instagram-link');
            } else if (naurls.includes('soundcloud')){
                links[i].classList.add('soundcloud-link');
            } else if (naurls.includes('bandcamp')){
                links[i].classList.add('bandcamp-link');
            }
        }
</script>