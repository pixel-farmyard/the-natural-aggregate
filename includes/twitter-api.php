<?php
/**
 * Twitter API
 *
 * @package twkmedia
 */

/**
 * Build the base string
 *
 * @param [type] $base_uri
 * @param [type] $method
 * @param [type] $params
 * @return void
 */
function build_base_string( $base_uri, $method, $params ) {
	$r = array();
	ksort( $params );

	foreach ( $params as $key => $value ) {
		$r[] = "$key=" . rawurlencode( $value );
	}

	return $method . '&' . rawurlencode( $base_uri ) . '&' . rawurlencode( implode( '&', $r ) );
}


/**
 * Build authorization header.
 *
 * @param [type] $oauth
 * @return void
 */
function build_authorization_header( $oauth ) {
	$r      = 'Authorization: OAuth ';
	$values = array();

	foreach ( $oauth as $key => $value )
		$values[] = "$key=\"" . rawurlencode($value) . "\"";

	$r .= implode( ', ', $values );

	return $r;
}

/**
 * Return Tweet.
 *
 * @param [type] $twitter_feed
 * @param [type] $count
 * @return void
 */
function return_tweet( $twitter_feed, $count ) {
	$oauth_access_token        = '1698755095-hojlVOQzALvc1V5X6NFusWHp5odwaEmXH4o3MHR'; // REPLACED.
	$oauth_access_token_secret = 'QYmQ0DoKHP26oPiY3sgXl5TkAueM5wRMnp2bmmt8mX5pL';      // REPLACED.
	$consumer_key              = '1OAxywOxpZYkC3fA938hwqKmb';                          // REPLACED.
	$consumer_secret           = 'yCX4yJnRq1pd8Z0vsSHg8Euh3zmsvCdukQz65u2S8hJOlAslqI'; // REPLACED.

	$request = array(
		'screen_name' => 'thenataggregate', // REPLACED.
		'count'       => $count,
		'tweet_mode'  => 'extended',
	);

	$oauth = array(
		'oauth_consumer_key'     => $consumer_key,
		'oauth_nonce'            => time(),
		'oauth_signature_method' => 'HMAC-SHA1',
		'oauth_token'            => $oauth_access_token,
		'oauth_timestamp'        => time(),
		'oauth_version'          => '1.0',
	);

	$oauth                    = array_merge( $oauth, $request );
	$base_info                = build_base_string( "https://api.twitter.com/1.1/$twitter_feed.json", 'GET', $oauth );
	$composite_key            = rawurlencode( $consumer_secret ) . '&' . rawurlencode( $oauth_access_token_secret );
	$oauth_signature          = base64_encode( hash_hmac( 'sha1', $base_info, $composite_key, true ) );
	$oauth['oauth_signature'] = $oauth_signature;
	$header                   = array(
		build_authorization_header( $oauth ),
		'Expect:',
	);
	$options                  = array(
		CURLOPT_HTTPHEADER     => $header,
		CURLOPT_HEADER         => false,
		CURLOPT_URL            => "https://api.twitter.com/1.1/$twitter_feed.json?" . http_build_query( $request ),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_SSL_VERIFYPEER => false,
	);

	$feed = curl_init();
	curl_setopt_array( $feed, $options );
	$json = curl_exec( $feed );
	curl_close( $feed );

	return json_decode( $json, true );
}

/**
 * Trim tweet
 *
 * @param [type] $trim
 * @return void
 */
function trim_tweet( $trim ) {
	// $trim = preg_replace( "/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $trim );
	 $trim = preg_replace( "/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $trim );
	 $trim = preg_replace( "/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $trim );

	return $trim;
}


// -----------------------------------------------
// Use the following code to display twitter posts:
// -----------------------------------------------
/* $tweets = return_tweet('statuses/user_timeline', '1'); // change number for amount of tweets you want to display.
foreach ( $tweets as $tweet ) {
	echo '<p>' . trim_tweet( $tweet['full_text']) . '</p>';
} */
