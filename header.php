<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- BASIC META -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Martin J Stephens">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"  />
<!-- TITLE -->
    <title><?php bloginfo('name'); ?> - 
            <?php is_front_page() ? bloginfo('description') : wp_title(); ?>
    </title>
    
<!-- GLOBAL SITE TAG (gtag.js) - GOOGLE ANALYTICS -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-146526295-1"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-146526295-1');
	</script>
    
<!-- ADDTHIS SCRIPT (Posts only) -->
<?php 
    $obj = get_queried_object();
	if($obj) {
    $type_is_post = $obj->post_type;
        if($type_is_post == "post"){
            echo '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e8e1a37dfe5f412"></script>';
    	}
	}
	?>
    
<!-- THE NATURAL AGGREGATE STYLE -->
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
	<link href="/wp-content/themes/thenaturalaggregate/magnific-popup.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
    
<?php wp_head(); ?>
</head>
<body>
<!-- GET MENU -->
    <nav class="mainmenu">
        <?php wp_nav_menu( array( 'simple' => 'simple-menu' ) ); ?>
    </nav>