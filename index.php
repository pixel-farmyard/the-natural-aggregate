/*
Theme Name: Natural Aggregate
Theme URI: http://thenaturalaggregate.co.uk
Author: Martin J Stephens
Author URI: http://martinjstephens.com
Description: Theme for thenaturalaggregate.co.uk
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: thenaturalaggregate
*/