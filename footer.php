<!-- TWITTER FEED -->
<?php
$value = get_field( "show_twitter_feed", false);

if( $value  ) {
    include('parts/twitter-feed.php');
} ?>

<footer>
        
<!-- LOGO LINK CENTER -->   
        
            <a class="logo" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?> </a>
            
<!-- SUBSCRIBE -->
<?php include('parts/sign-up-form.php'); ?>
    
<!--  SOCIAL ICONS -->
        
    <nav class="socials">
                    <a target="_blank" href="https://www.instagram.com/thenataggregate/"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/instagram-icon.svg'); ?></a>
                    <a target="_blank" href="https://vimeo.com/thenaturalaggregate"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/vimeo-icon.svg'); ?></a>
                    <a target="_blank" href="https://twitter.com/TheNatAggregate"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/twitter-icon.svg'); ?></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCHgi3YrsBBo_8AgB0TcDkGg"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/youtube-icon.svg'); ?></a>
    </nav>

<!-- FOOTER BOTTOM -->        
<div class="lower-footer">
    <p>Copyright &copy; <?php echo date( ' Y ' ); ?> The Natural Aggregate &emsp; | &emsp; <span style="white-space:nowrap"><a href="http://thenaturalaggregate.co.uk/privacy-and-cookies-policy/">Cookies and Privacy Policy</a></span></p></div>
                
</footer>

<!-- ADD JQUERY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script> 
<!--<script src="https://code.jquery.com/jquery-1.12.4.js" crossorigin="anonymous"></script> -->

<!-- ADD MAGNIFIC POP-UP -->
<script src="/wp-content/themes/thenaturalaggregate/jquery.magnific-popup.js"></script>

<!-- ADD NAT AGG JAVASCRIPT -->
<script src="/wp-content/themes/thenaturalaggregate/javascript.js"></script>
   
</body>
</html> 