// PRELOADER
if (!sessionStorage.isVisited) {
  $('body').addClass('preloading');
  window.addEventListener('load', function() {
  sessionStorage.isVisited = 'true' 
  $("#nat-preloader").delay(2000).fadeOut("1000")
  setTimeout(function() {
       $('body').removeClass("preloading");
   }, 2000);
  window.scrollTo(0,0);
  });
} else {
  $("#nat-preloader").hide()
  $('body').removeClass('preloading')
}

// START POP-UP PLAYERS FOR VIMEO & BANDCAMP
$('[href*="https://vimeo.com/"]').addClass('video-popup-link');
$('[href*="https://bandcamp.com/"]').addClass('bandcamp-popup-link');
$('#bandcamp-popup').addClass('open-popup-link');

$('.video-popup-link').magnificPopup({
  type: 'iframe'
});

$('.bandcamp-popup-link').magnificPopup({
  type: 'iframe'
});

$('.open-popup-link').magnificPopup({
  type:'inline',
  midClick: true
});
// POP-UP PLAYERS END

$( document ).ready(function() {
    $('.notfoundfill').addClass('executefill');
});

document.addEventListener('DOMContentLoaded', function(e) {
  var link = document.querySelector('#homepage');
  if (link && link.href == window.location.href) { 
    var upperTeaser = $('.music-upper-teaser');
    if (upperTeaser.attr('style').indexOf('wp-content') > -1) {
        $('.music-upper-teaser').addClass('plus-gradient');
    } 
  }
});

// TEASER ANIMATION ADD CLASS
$(function(){
    $('.blog-teaser').hover(function(){
        $('.read-more').toggleClass('active');
    });
});

// ADD TEXT LINK CLASS
$('.post-body a').addClass('text-link');
$('.category-intro a').addClass('text-link');

