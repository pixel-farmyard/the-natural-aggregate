<?php get_header(); ?>

<!-- GET PRELOADER -->
<div id="nat-preloader">
    <?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/roots.svg'); ?>
</div>

<!-- GET FEATURED IMAGE FOR HERO BG -->
<?php
                if (has_post_thumbnail()) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                $thumbnail_url = $thumbnail_data[0];
                }
        ?>

<!-- HERO IMAGES -->
<?php $thisPage = get_post_field( 'post_name', get_post() ); ?>
<section class="showcase" style="background-image:url('<?php echo $thumbnail_url ?>')">
            <a id="homepage" href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/full-logo.svg'); ?></a>
<!-- HERO TITLE -->
    <h1 id="home-heading-one"><?php is_front_page() ? bloginfo('name') : the_title(''); ?></h1>
    </section>
        <?php if(have_posts()) : ?>
             <?php while(have_posts()) : the_post(); ?>

<!-- THE CONTENT AS PAGE INTRO -->        
<section class="music-intro">
        <?php the_content(); ?> 
        
            
    <?php endwhile; ?>
    <?php endif; ?>        
</section>
    
<!-- CONTENT FROM UPPER TEASER ACF -->
    <section class="music-upper-teaser" style="background-image:url(<?php echo get_field('upper_teaser_image'); ?>)">
        <?php 
        $videofile = get_field("background_video", false);
        if( $videofile ) {
            echo '<video autoplay muted loop id="upper-bg-video">';
            echo    '<source src="' . $videofile . '" type="video/mp4">';
            echo '</video>';
        } ?> 
        <p><?php the_field('upper_teaser_text'); ?></p>
        <?php
        $link = get_field('upper_teaser_link');
        if( $link ): 
	       $link_url = $link['url'];
	       $link_title = $link['title'];
	       $link_target = $link['target'] ? $link['target'] : '_self';
	        ?>
	    <a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
    </section>

<!-- CONTENT FROM FEATURED POSTS ACF -->
<?php include('parts/featured-posts.php'); ?>

<!-- CONTENT FROM LOWER TEASER ACF -->
<?php include('parts/lower-teaser.php'); ?>

<?php get_footer(); ?>