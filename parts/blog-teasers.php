<?php $prev_post_exists = get_previous_post();
if ($prev_post_exists) { ?>
<div class="blog-teasers-wrapper">
<?php
global $post;
$current_post = $post; // remember the current post

for($i = 1; $i <= 2; $i++){
  $post = get_previous_post(); // this uses $post->ID
  setup_postdata($post); 
    if($post) {  ?>

    <div class="blog-teaser" style="background-image:url('<?php echo the_post_thumbnail_url( 'large' ); ?>');">
        <a href="<?php the_permalink() ?>">
        <div class="teaser-text-wrapper">
        <h2><?php the_title(); ?></h2>
        <p><?php the_excerpt() ?></p>
        <span class="read-more">Read more</span>
        </div>
        </a>
    </div>  

<?php }
    }

$post = $current_post;
?>
</div> 
<?php } ?>

<section class="music-final post-final">
        <div class="forward-back">
            <a href="/research">Back to research</a>
        </div>
</section> 
    