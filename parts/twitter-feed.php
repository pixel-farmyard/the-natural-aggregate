<section class="twitter-feed">
<div class="tweet-inner">
<p><a class="twitter-small" href="https://twitter.com/TheNatAggregate" target="_blank">@TheNatAggregate</a></p>
<?php $tweets = return_tweet('statuses/user_timeline', '1'); // change number for amount of tweets you want to display.

foreach ( $tweets as $tweet ) {
	echo '<p>' . trim_tweet( $tweet['full_text']) . '</p>';
	$time = strtotime( $tweet['created_at'] );
    $tweet_date = date( 'j F Y', $time );
    echo '<p class="twitter-small">' . $tweet_date . '</p>';
} 
?>
<a class="button" href="https://twitter.com/TheNatAggregate" target="_blank">Follow</a></div>
</section>