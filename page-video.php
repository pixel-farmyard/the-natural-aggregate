<?php
/*
Template Name: Video
*/ ?>

<?php get_header(); ?>   
<!-- GET FEATURED IMAGE FOR HERO BG -->
<?php
                if (has_post_thumbnail()) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                $thumbnail_url = $thumbnail_data[0];
                }
        ?>

<!-- HERO IMAGES -->
<?php $thisPage = get_post_field( 'post_name', get_post() ); ?>
<section class="showcase" style="background-image:url('<?php echo $thumbnail_url ?>')">
            <a href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/roots.svg'); ?></a>
<!-- HERO TITLE -->        
    <h1><?php is_front_page() ? bloginfo('name') : the_title(''); ?></h1>
    </section>
        <?php if(have_posts()) : ?>
             <?php while(have_posts()) : the_post(); ?>

<!-- THE CONTENT AS PAGE INTRO --> 
        <section class="music-intro">
        <?php the_content(); ?> 
            
    <?php endwhile; ?>
    <?php endif; ?>        
    </section>

<!-- GET POSTS BY CATEGORY -->
    <section class="music-main bg-shade">
        <?php
$loop = new WP_Query(
    array(
        'post_type' => 'Video' // This is the name of your post type - change this as required,
    )
);
while ( $loop->have_posts() ) : $loop->the_post();
                if (has_post_thumbnail()) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                $thumbnail_url = $thumbnail_data[0];
                }
        ?>
        <div class="music-box">
        <div class="music-box-image" style="background-image:url('<?php echo $thumbnail_url ?>')"></div>
        
        <div class="music-box-content">
        <h3><?php the_title();?></h3>
        <?php the_content(); ?>
        </div>
    </div>
 
<?php endwhile;
wp_reset_postdata();
?>
    </section>
    
<!-- CONTENT FROM LOWER TEASER ACF -->
<?php include('parts/lower-teaser.php'); ?>
    
<?php get_footer(); ?>