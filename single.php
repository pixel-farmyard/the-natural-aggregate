<?php get_header(); ?>    
<!-- GET FEATURED IMAGE FOR HERO BG -->
<?php
                if (has_post_thumbnail()) {
                $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                $thumbnail_url = $thumbnail_data[0];
                }
        ?>
<!-- HERO IMAGES -->
<section class="banner" style="background-image:url('<?php echo $thumbnail_url ?>')">
            <a href="<?php echo home_url(); ?>">
            <?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/roots.svg'); ?></a>
    </section> 
<!-- GET ACF LINK / SELECTION COLOUR -->
<?php 
$linkcol = get_field('colour_select', $post->ID);
if( $linkcol ) {
    $linkbg = strtolower($linkcol);
} else {
    $linkbg ='';
} ?>

<?php echo '<section class="post-body '.$linkbg. '">'; ?>
<!-- LOOP -->  
        <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post(); ?>
<!-- TITLE CONTENT AND META -->
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <p class="tna-post-meta">
                Posted <?php echo get_the_date('d - m - y'); ?>
                <?php wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
            </p>
        <?php endwhile; ?>
        <?php else : ?>
            <p><?php __('Sorry, there are no posts...'); ?></p>
        <?php endif; ?>
</section>
<!-- NOTES SECTION -->
<?php if( get_field('notes_content') ): ?>
	<section class="post-notes">
        <h2>Notes</h2>
        <?php the_field('notes_content'); ?>
    </section>
<?php endif; ?>
    
<!-- PREV NEXT AND BACK BUTTONS -->
<?php  include('parts/blog-teasers.php'); ?> 

<?php get_footer(); ?>  