

<?php

require get_template_directory() . '/includes/twitter-api.php';
   
 function tna_theme_setup(){
// ADD FEATURED IMAGE 
    add_theme_support('post-thumbnails');
    add_image_size('thumb-size',580, 480, true);
     
// REGISTER MENU 
    register_nav_menus( array( 
        'simple' => __('Simple menu')
    )); 
 }
add_action( 'after_setup_theme', 'tna_theme_setup' );


// EXCERPT LENGTH CONTROL
function set_excerpt_length(){
    return 50;
}
add_filter('excerpt_length', 'set_excerpt_length');
add_filter('show_admin_bar', '__return_false');

// ADD CLASS TO POST LINKS
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
  return 'class="button skip-buttons"';
}

// ADDING CUSTOM WYSIWYG STYLE
function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {  

    $style_formats = array(  
        // These are the custom styles
        array(  
            'title' => 'H3',  
            'block' => 'h3',  
            'wrapper' => true,
        ),
        array(  
            'title' => 'Button',  
            'selector' => 'a',  
            'classes' => 'button',
        )
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
    
    return $init_array;  
  
} 

// ADD EDITOR STYLES
function custom_editor_styles() {
	add_editor_style('editor-styles.css');
 }

 add_action('init', 'custom_editor_styles');


// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

add_filter( 'mce_buttons', 'remove_tiny_mce_buttons_from_editor');
function remove_tiny_mce_buttons_from_editor( $buttons ) {

    $remove_buttons = array(
        'formatselect',
        'wp_more',
    );
    foreach ( $buttons as $button_key => $button_value ) {
        if ( in_array( $button_value, $remove_buttons ) ) {
            unset( $buttons[ $button_key ] );
        }
    }
    return $buttons;
}


// REGISTER CPTS
function tna_custom_post_type() {
    register_post_type('music',
        array(
            'labels'      => array(
                'name'          => __('Music', 'textdomain'),
                'singular_name' => __('Music', 'textdomain'),
            ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-album',
                'supports' => array('title','editor','thumbnail'),
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'capability_type'    => 'post',
                'hierarchical'       => false,
                'menu_position'      => null,
        )
    );
     register_post_type('video',
        array(
            'labels'      => array(
                'name'          => __('Video', 'textdomain'),
                'singular_name' => __('Video', 'textdomain'),
            ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-format-video',
                'supports' => array('title','editor','thumbnail'),
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'capability_type'    => 'post',
                'hierarchical'       => false,
                'menu_position'      => null,
        )
    );
}
add_action('init', 'tna_custom_post_type');

// CUSTOM DASHBOARD WIDGET
add_action('wp_dashboard_setup', 'custom_dashboard_widget');
  
function custom_dashboard_widget() {
global $wp_meta_boxes;
 
wp_add_dashboard_widget('nat-agg-links-widget', 'The Natural Aggregate', 'custom_dashboard_help');
}
 
function custom_dashboard_help() {
echo file_get_contents(get_template_directory_uri() . '/includes/nat-agg-links-widget.php');
}

// ADMIN MENU AMENDS
function remove_menus(){
  remove_menu_page( 'link-manager.php' );    //Links
  remove_menu_page( 'edit-comments.php' );    //Comments
}
add_action( 'admin_menu', 'remove_menus' );


// CUSTOMISE ACF
function my_acf_admin_head() {
    ?>
    <style type="text/css">
        div.acf-field.colour-select .acf-input ul li label {
            font-weight:bold;
        }
        
        div.acf-field.colour-select .acf-input ul li label::before {
            display:inline-block;
            width:16px;
            height:16px;
            content: " ";
            background-color:#ccc;
            margin-right:10px;
            margin-bottom:-4px;
            margin-top:4px;
        }
        
        div.acf-field.colour-select .acf-input ul li:nth-child(1) > label::before {
            background-color:rgb(255,90,90);
        }
        div.acf-field.colour-select .acf-input ul li:nth-child(2) > label::before {
            background-color:rgb(90,255,90);
        }
        div.acf-field.colour-select .acf-input ul li:nth-child(3) > label::before {
            background-color:rgb(90,90,255);
        }
        div.acf-field.colour-select .acf-input ul li:nth-child(4) > label::before {
            background-color:rgb(220,220,90);
        }

    </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

// ACF JS API
function my_admin_enqueue_scripts() {
	wp_enqueue_script( 'my-admin-js', get_template_directory_uri() . 'assets/js/acf-extension.js', array(), '1.0.0', true );
}

add_action('acf/input/admin_enqueue_scripts', 'my_admin_enqueue_scripts');