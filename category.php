<?php get_header(); ?>    


<?php $term = get_queried_object(); ?>
<?php $image = get_field('banner_image', $term); ?>

<!-- HERO IMAGES -->
<?php $thisPage = get_post_field( 'post_name', get_post() ); ?>
<section class="showcase" style="background-image:url('<?php echo $image['url']; ?>')">
           <a href="<?php echo home_url(); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/svg/roots.svg'); ?></a>
<!-- HERO TITLE -->        
    <h1>Category: <?php echo single_cat_title(); ?></h1>    
</section>

<!-- GET ACF LINK / SELECTION COLOUR -->
<?php 
$linkcol = get_field('colour_select', $term);
if( $linkcol ) {
    $linkbg = strtolower($linkcol);
} else {
    $linkbg ='';
} ?>

<!-- THE CONTENT AS PAGE INTRO -->
<?php if ( !empty( category_description() ) ){
    echo "<section class='music-intro category-intro " . $linkbg . "'>";
    echo category_description();    
    echo "</section>";
}; ?>

<!-- GET POSTS -->
    <section class="music-main bg-shade">
<?php   

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';        
$loop = new WP_Query(
    array (
                'nopaging'               => false,
                'paged'                  => $paged,
                'posts_per_page'         => '5',
                'post_type'              => 'post',
    )
);
?>
        
        <?php if(have_posts()) : ?>
             <?php while(have_posts()) : the_post(); 
                    if (has_post_thumbnail()) {
                        $thumbnail_data = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
                        $thumbnail_url = $thumbnail_data[0];
                }
            ?>
        <div class="music-box">
        <div class="music-box-image" style="background-image:url('<?php echo $thumbnail_url ?>')">
            <div class="category-tags"><?php 
                $categories = get_the_category();
                    foreach( $categories as $category) {
                    $name = $category->name;
                    $category_link = get_category_link( $category->term_id );
                // wrap in a link with href to $category_link when you can
                echo "<a href=" . $category_link . "> <span class=" . esc_attr( $name) . ">" . esc_attr( $name) . " </span></a>";}
                ?></div>
        </div>
        
        <div class="music-box-content">
        <h3><?php the_title();?></h3>
        <?php the_excerpt(); ?>
        <a class="button" href="<?php echo the_permalink(); ?>">READ</a>
        </div>
    </div>
        
    <?php 
    endwhile;
    endif;          
        previous_posts_link( '« Newer' );
        next_posts_link( 'Older »', $loop->max_num_pages );    
    wp_reset_postdata();
    ?>
    </section>

<?php get_footer(); ?>  